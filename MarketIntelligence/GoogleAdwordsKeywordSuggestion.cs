﻿// Copyright 2017, Google Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.AdWords.v201702;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MarketIntelligence
{
	/// <summary>
	/// This code example retrieves keywords that are related to a given keyword.
	/// </summary>
	public class GoogleAdwordsKeywordSuggestion
	{
		private static AdWordsUser adwordsUser = new AdWordsUser();
		public static void Mainx(string[] args)
		{
			GoogleAdwordsKeywordSuggestion codeExample = new GoogleAdwordsKeywordSuggestion();
			try
			{
				GetKeywordsUsingSeedKeywords(null);
			}
			catch (Exception e)
			{
				Console.WriteLine("An exception occurred while running this code example." + e);
			}
			Console.ReadLine();
		}

		/// <summary>
		/// Runs the code example.
		/// </summary>
		/// <param name="adwordsUser">The AdWords user.</param>
		public static List<KeywordModel> GetKeywordsUsingSeedKeywords(HashSet<string> seedKeywords)
		{
			List<KeywordModel> keywordList = new List<KeywordModel>();

			// Get the TargetingIdeaService.
			TargetingIdeaService targetingIdeaService =
				(TargetingIdeaService) adwordsUser.GetService(AdWordsService.v201702.TargetingIdeaService);

			Paging paging = Paging.Default;

			// Create selector.
			TargetingIdeaSelector selector = new TargetingIdeaSelector();
			selector.requestType = RequestType.IDEAS;
			selector.ideaType = IdeaType.KEYWORD;
			selector.localeCode = "en_IN";
			selector.currencyCode = "USD";
			//var paging = new Paging { startIndex = 0, numberResults = 500 };
			selector.paging = paging;
			selector.requestedAttributeTypes = new AttributeType[] {
			AttributeType.CATEGORY_PRODUCTS_AND_SERVICES,
			AttributeType.COMPETITION,
			AttributeType.EXTRACTED_FROM_WEBPAGE,
			AttributeType.IDEA_TYPE,
			AttributeType.KEYWORD_TEXT,
			AttributeType.SEARCH_VOLUME,
			AttributeType.AVERAGE_CPC,
			AttributeType.TARGETED_MONTHLY_SEARCHES};

			// Create related to query search parameter.
			RelatedToQuerySearchParameter relatedToQuerySearchParameter =
				new RelatedToQuerySearchParameter();
			relatedToQuerySearchParameter.queries = seedKeywords.ToArray();

			// Add a language search parameter (optional).
			// The ID can be found in the documentation:
			//   https://developers.google.com/adwords/api/docs/appendix/languagecodes
			LanguageSearchParameter languageParameter = new LanguageSearchParameter();
			Language english = new Language();
			english.id = 1000;
			languageParameter.languages = new Language[] { english };


			Location lc = new Location() { id = 2356L };
			LocationSearchParameter lsp = new LocationSearchParameter() { locations = new Location[] { lc } };


			// Add network search parameter (optional).
			NetworkSetting networkSetting = new NetworkSetting();
			networkSetting.targetGoogleSearch = true;
			networkSetting.targetSearchNetwork = false;
			networkSetting.targetContentNetwork = false;
			networkSetting.targetPartnerSearchNetwork = false;

			NetworkSearchParameter networkSearchParameter = new NetworkSearchParameter();
			networkSearchParameter.networkSetting = networkSetting;

			SearchVolumeSearchParameter searchVolumeParam = new SearchVolumeSearchParameter() { operation = new LongComparisonOperation() { minimum = 1000 } };


			// Set the search parameters.
			selector.searchParameters = new SearchParameter[] {
		  relatedToQuerySearchParameter, languageParameter, networkSearchParameter,searchVolumeParam,lsp};

			// Set selector paging (required for targeting idea service).

			TargetingIdeaPage page = new TargetingIdeaPage();
			try
			{
				int i = 0;
				do
				{
					// Get related keywords.
					page = targetingIdeaService.get(selector);

					// Display related keywords.
					if (page.entries != null && page.entries.Length > 0)
					{
						foreach (TargetingIdea targetingIdea in page.entries)
						{
							KeywordModel keywordModelObj = new KeywordModel();
							foreach (Type_AttributeMapEntry entry in targetingIdea.data)
							{
								switch (entry.key)
								{
									case AttributeType.AVERAGE_CPC:
										MoneyAttribute cpc = (entry.value as MoneyAttribute);
										if (cpc != null && cpc.value != null)
											keywordModelObj.CostPerClick = (cpc.value.microAmount / 10000) / 100.0;
										break;

									case AttributeType.CATEGORY_PRODUCTS_AND_SERVICES:
										IntegerSetAttribute categorySet = entry.value as IntegerSetAttribute;
										StringBuilder builder = new StringBuilder();
										if (categorySet.value != null)
											keywordModelObj.Categories = categorySet.value;
										break;

									case AttributeType.COMPETITION:
										keywordModelObj.Competition = (entry.value as DoubleAttribute)?.value ?? 0;
										break;

									case AttributeType.EXTRACTED_FROM_WEBPAGE:
										keywordModelObj.ExtractedFromWebpage = (entry.value as StringAttribute)?.value;
										break;

									case AttributeType.IDEA_TYPE:
										keywordModelObj.IdeaType = (entry.value as IdeaTypeAttribute).value.ToString();
										break;

									case AttributeType.KEYWORD_TEXT:
										keywordModelObj.Keyword = (entry.value as StringAttribute)?.value;
										break;

									case AttributeType.SEARCH_VOLUME:
										keywordModelObj.AverageMonthlySearches = (entry.value as LongAttribute)?.value ?? 0;
										break;

									case AttributeType.TARGETED_MONTHLY_SEARCHES:
										MonthlySearchVolume[] monthlySearchVolumeArray = (entry.value as MonthlySearchVolumeAttribute).value;
										if (monthlySearchVolumeArray != null && monthlySearchVolumeArray.Length > 0)
										{
											keywordModelObj.TargetedMonthlySearches = new List<MonthlySearchVolumeModel>();
											foreach (MonthlySearchVolume monthlySearchVolumeObj in monthlySearchVolumeArray)
											{
												MonthlySearchVolumeModel msvModelObj = new MonthlySearchVolumeModel();
												msvModelObj.Month = monthlySearchVolumeObj.month;
												msvModelObj.Year = monthlySearchVolumeObj.year;
												msvModelObj.Count = monthlySearchVolumeObj.count;
												keywordModelObj.TargetedMonthlySearches.Add(msvModelObj);
											}
										}
										break;
								}
							}
							//Console.WriteLine(keywordModelObj.ToJson());
							//Console.WriteLine(i + "\t" + keywordModelObj.CostPerClick + "\t|\t" + keywordModelObj.Keyword + "\t|\t" + keywordModelObj.AverageMonthlySearches+"\t|\t"+keywordModelObj.Competition);
							Console.WriteLine(keywordModelObj.Keyword + "\t" + keywordModelObj.AverageMonthlySearches);
							keywordList.Add(keywordModelObj);
							i++;
						}
					}
					selector.paging.IncreaseOffset();
				} while (selector.paging.startIndex < page.totalNumEntries);
				Console.WriteLine("Number of related keywords found: {0}", page.totalNumEntries);
			}
			catch (Exception e)
			{
				throw e;
			}

			if (keywordList == null || keywordList.Count == 0)
				return null;


			keywordList = keywordList.OrderByDescending(x => x.AverageMonthlySearches).ToList();

			return keywordList;
		}
	}
}