﻿using MongoDB.Bson;
using MongoDB.Driver;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;

namespace MarketIntelligence.Database
{
	public static class DBConnectionManager
	{
		private static readonly Logger logger = LogManager.GetCurrentClassLogger();
		public static Object GetConnection(DBConnectionParameters dbParamObj)
		{
			if (dbParamObj == null || string.IsNullOrWhiteSpace(dbParamObj.Dbtype))
				return null;

			// Checking database type before connection
			switch (dbParamObj.Dbtype.ToLower())
			{
				// If database is mongo or mongodb, then create a new connection
				case "mongo":
				case "mongodb":
					IMongoClient mongoClient = null;
					try
					{
						// Primary Mode of creating a connection is through connection string
						// Connection string has format
						// mongodb://[username:password@]host1[:port1][,host2[:port2],...[,hostN[:portN]]][/[database][?options]]
						// E.g mongodb://user1:pwd123@ds00000-a1.mongolab.com:53034,ds053034-a2.mongolab.com:53034/testdb?replicaSet=rs-ds053034",
						if (!string.IsNullOrWhiteSpace(dbParamObj.ConnectionString))
						{
							try
							{
								mongoClient = new MongoClient(dbParamObj.ConnectionString);
							}
							catch (Exception innerException)
							{
								mongoClient = null;
								Console.WriteLine(innerException.StackTrace);
							}
						}

						/* If ConnectionString is unavailable or invalid, then try connecting through combination of
							host+user name+password+database for validation
						*/
						if (mongoClient == null && dbParamObj.Host != null)
						{
							try
							{
								MongoCredential credential = MongoCredential.CreateCredential(dbParamObj.Dbname, dbParamObj.User, dbParamObj.Password);
								List<MongoCredential> credentialList = new List<MongoCredential>();
								credentialList.Add(credential);

								MongoClientSettings clientSettings = new MongoClientSettings();
								clientSettings.Credentials = credentialList;
								clientSettings.Server = new MongoServerAddress(dbParamObj.Host, Int32.Parse(dbParamObj.Port));

								mongoClient = new MongoClient(clientSettings);
							}
							catch (Exception innerException)
							{
								mongoClient = null;
								Console.WriteLine(innerException.StackTrace);
							}
						}

						// If mongo_client is still null, then print error
						if (mongoClient == null)
						{
							logger.Error("ERROR: Both ConnectionString and Host strings are either null or invalid for database " + dbParamObj.ConnectionName + " configuration");
							Console.WriteLine("ERROR: Both ConnectionString and Host strings are either null or invalid for database " + dbParamObj.ConnectionName + " configuration");
							break;
						}
						return mongoClient;
					}
					catch (Exception e)
					{
						logger.Error(e.StackTrace);
						Console.WriteLine(e.StackTrace);
					}
					break;


				// If database is aurora or mysql, then create a new connection
				case "aurora":
				case "mysql":
				case "mysqldb":
					MySqlConnection mysqlConnectionObj = null;
					try
					{
						if (string.IsNullOrWhiteSpace(dbParamObj.ConnectionString))
							dbParamObj.ConnectionString = "SERVER=" + dbParamObj.Host + ";PORT=" + dbParamObj.Port + ";DATABASE=" + dbParamObj.Dbname + ";UID=" + dbParamObj.User + ";PASSWORD=" + dbParamObj.Password + ";MAX POOL SIZE=1000;";

						Console.WriteLine(dbParamObj.ConnectionString);
						mysqlConnectionObj = new MySqlConnection(dbParamObj.ConnectionString);
						return mysqlConnectionObj;
					}
					catch (Exception e)
					{
						logger.Error("Unable to create MySQL Connection\nError:" + e.Message + "\n" + e.StackTrace);
						Console.WriteLine(e.StackTrace);
					}
					break;

				default:
					Console.WriteLine("Database type must be mongo or mysql");
					break;
			}
			return null;

		}


		public static MySqlConnection GetMySqlConnection(string connectionName)
		{
			string connectionString = null;

			try
			{
				connectionString = ConfigurationManager.ConnectionStrings[connectionName]?.ConnectionString;
				if (connectionString != null)
				{
					DBConnectionParameters dbParamObj = new DBConnectionParameters();
					dbParamObj.ConnectionName = connectionName;
					dbParamObj.ConnectionString = connectionString;
					dbParamObj.Dbtype = "aurora";
					return (MySqlConnection) GetConnection(dbParamObj);
				}
			}
			catch (Exception e)
			{
				logger.Error(e);
			}
			return null;
		}
		public static IMongoClient GetMongoDBConnection(string connectionName)
		{
			string connectionString = null;

			try
			{
				connectionString = ConfigurationManager.ConnectionStrings[connectionName]?.ConnectionString;
				if (connectionString != null)
				{
					DBConnectionParameters dbParamObj = new DBConnectionParameters();
					dbParamObj.ConnectionName = connectionName;
					dbParamObj.ConnectionString = connectionString;
					dbParamObj.Dbtype = "mongodb";
					return (IMongoClient) GetConnection(dbParamObj);
				}
			}
			catch (Exception e)
			{
				logger.Error(e);
			}
			return null;
		}
	}
}
