﻿using Newtonsoft.Json;

namespace MarketIntelligence
{

	public class DBConnectionParameters
	{

		[JsonProperty("connectionName")]
		public string ConnectionName { get; set; }

		[JsonProperty("connectionString")]
		public string ConnectionString { get; set; }

		[JsonProperty("host")]
		public string Host { get; set; }

		[JsonProperty("dbName")]
		public string Dbname { get; set; }

		[JsonProperty("user")]
		public string User { get; set; }

		[JsonProperty("password")]
		public string Password { get; set; }

		[JsonProperty("dbType")]
		public string Dbtype { get; set; }

		[JsonProperty("port")]
		public string Port { get; set; }
	}

}
