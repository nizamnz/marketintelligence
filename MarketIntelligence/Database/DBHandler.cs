﻿using MongoDB.Driver;
using MovingFloats.Server.Model;
using MySql.Data.MySqlClient;
using NLog;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using MarketIntelligence.Models;

namespace MarketIntelligence.Database
{
	public class DBHandler
	{
		private static Logger logger = LogManager.GetCurrentClassLogger();

		private static ConcurrentDictionary<string, FloatingPointFinal> fpDictionary = new ConcurrentDictionary<string, FloatingPointFinal>();

		// Floating Points //
		public static long RetrieveNumberOfRecordsFromFloatingPointsInDateRange(DateTime startDate, DateTime endDate, string connectionName = "floatdb", string database = "floatdb", string collection = "floatingpoints")
		{
			try
			{
				// Creates a MongoClient and MongoDatabase
				IMongoClient imongoClient = DBConnectionManager.GetMongoDBConnection(connectionName);
				IMongoDatabase floatdb = imongoClient.GetDatabase(database);


				// Swapping dates if start date occurs after end date
				if (startDate > endDate)
				{
					var temp = startDate;
					startDate = endDate;
					endDate = temp;
				}

				// Creates an object of MongoCollection conforming to model class FloatingPointFinal
				IMongoCollection<FloatingPointFinal> floatingPointCollection = floatdb.GetCollection<FloatingPointFinal>(collection);

				// Creating Filter for Date Range Greater than Equal to Start Date and Less than End Date
				var builder = Builders<FloatingPointFinal>.Filter;
				//var dateFilter = builder.Gte("CreatedOn", startDate) & builder.Lt("CreatedOn", endDate);
				var dateFilter = builder.And(builder.Gte("CreatedOn", startDate), builder.Lt("CreatedOn", endDate), builder.Eq("IsStoreFront", true), builder.Or(builder.Lte("PaymentLevel", 10), builder.And(builder.Gt("PaymentLevel", 10), builder.Gte("ExpiryDate", DateTime.Today.Subtract(TimeSpan.FromDays(30 * 3))))));
				var cursor = floatingPointCollection.Find(dateFilter);
				long numOfRecords = cursor.Count();

				return numOfRecords;
			}
			catch (Exception e)
			{
				logger.Error(e, "Error Retrieving Number of Records from FloatingPoint between " + startDate + " AND " + endDate);
				Console.WriteLine("Error Retrieving Number of Records from FloatingPoint between " + startDate + " AND " + endDate);
				Console.WriteLine(e.Message + "\n" + e.StackTrace);
			}
			return 0;
		}

		public static List<FloatingPointFinal> RetrieveRecordsFromFloatingPointsInDateRange(DateTime startDate, DateTime endDate, string connectionName = "floatdb", string database = "floatdb", string collection = "floatingpoints", int limit = -1, int offset = 0)
		{
			try
			{
				// Creates a MongoClient and MongoDatabase
				IMongoClient imongoClient = DBConnectionManager.GetMongoDBConnection(connectionName);
				IMongoDatabase floatdb = imongoClient.GetDatabase(database);

				// Swapping dates if start date occurs after end date
				if (startDate > endDate)
				{
					var temp = startDate;
					startDate = endDate;
					endDate = temp;
				}

				// Creates an object of MongoCollection conforming to model class FloatingPointFinal
				IMongoCollection<FloatingPointFinal> floatingPointsCollection = floatdb.GetCollection<FloatingPointFinal>(collection);

				// Creating Filter for Date Range Greater than Equal to Start Date and Less than End Date
				var builder = Builders<FloatingPointFinal>.Filter;
				var dateFilter = builder.And(builder.Gte("CreatedOn", startDate), builder.Lt("CreatedOn", endDate), builder.Eq("IsStoreFront", true), builder.Or(builder.Lte("PaymentLevel", 10), builder.And(builder.Gt("PaymentLevel", 10), builder.Gte("ExpiryDate", DateTime.Today.Subtract(TimeSpan.FromDays(30 * 3))))));
				//var dateFilter = builder.Gte("CreatedOn", startDate) & builder.Lt("CreatedOn", endDate) & builder.Eq("IsStoreFront", true);

				// DateTime objects time1 and time2 are used to calculate time taken to retrieve records between 
				List<FloatingPointFinal> list;
				DateTime time1 = DateTime.Now;

				// Retrieving records, if no/invalid limit is specified then all records are retrieved otherwise records as per specified limit and offset are retrieved
				if (limit < 0)
					list = floatingPointsCollection.Find(dateFilter).Project<FloatingPointFinal>(Builders<FloatingPointFinal>.Projection.Include("Tag").Include("Category").Include("RootAliasUri").Include("CreatedOn")).ToList();
				else
					list = floatingPointsCollection.Find(dateFilter).Skip(offset).Limit(limit).ToList();

				DateTime time2 = DateTime.Now;
				TimeSpan timeDifference = time2.Subtract(time1);
				logger.Info("Time taken to retrieve " + list.Count + " records between \n " + startDate.ToShortDateString() + " and " + endDate.ToShortDateString() + " = " + (timeDifference.Hours == 0 ? "" : timeDifference.Hours + " hours ") + (timeDifference.Minutes == 0 ? "" : timeDifference.Minutes + " minutes ") + (timeDifference.Seconds == 0 ? "" : timeDifference.Seconds + "seconds") + (timeDifference.Milliseconds == 0 ? "" : timeDifference.Milliseconds + " milliseconds"));
				return list;
			}
			catch (Exception e)
			{
				logger.Error(e, "Error Retrieving Records from FloatingPoint between " + startDate + " AND " + endDate);
				Console.WriteLine("Error Retrieving Records from FloatingPoint between " + startDate + " AND " + endDate);
				Console.WriteLine(e.Message + "\n" + e.StackTrace);
			}
			return null;
		}


		public static FloatingPointFinal RetrieveRecordFromFloatingPointsUsingFPTag(string fPTag, string connectionName = "floatdb", string database = "floatdb", string collection = "floatingpoints")
		{
			try
			{
				if (fpDictionary.ContainsKey(fPTag))
					return fpDictionary[fPTag];

				// Creates a MongoClient and MongoDatabase
				IMongoClient imongoClient = DBConnectionManager.GetMongoDBConnection(connectionName);
				IMongoDatabase floatdb = imongoClient.GetDatabase(database);

				// Creates an object of MongoCollection conforming to model class FloatingPointFinal
				IMongoCollection<FloatingPointFinal> floatingPointsCollection = floatdb.GetCollection<FloatingPointFinal>(collection);

				// Creating Filter for FPTag which must be equal to provided FPTag
				var builder = Builders<FloatingPointFinal>.Filter;
				var fPTagFilter = builder.Eq("Tag", fPTag);



				// DateTime objects time1 and time2 are used to calculate time taken to retrieve records between 
				DateTime time1 = DateTime.Now;

				// Retrieving 1 record containing details for Floating Point - City,Category,Country,CreatedOn
				FloatingPointFinal fpLimitedSetObj = floatingPointsCollection.Find(fPTagFilter).Project<FloatingPointFinal>(Builders<FloatingPointFinal>.Projection.Include("City").Include("Tag").Include("Category").Include("ApplicationId").Include("Country").Include("CreatedOn").Include("RootAliasUri")).FirstOrDefault();

				DateTime time2 = DateTime.Now;
				TimeSpan timeDifference = time2.Subtract(time1);

				if (fpLimitedSetObj != null)
					fpDictionary.TryAdd(fPTag, fpLimitedSetObj);

				return fpLimitedSetObj;
			}
			catch (Exception e)
			{
				logger.Error(e, "Error Retrieving Record from FloatingPointsCollection for FPTag:" + fPTag);
				Console.WriteLine("Error Retrieving Record from FloatingPointsCollection for FPTag:" + fPTag);
				Console.WriteLine(e.Message);
			}
			return null;
		}


		//Products

		public static List<FloatingPointProductFinal> RetrieveProductsFromProductsCollection(string fpTag = "", string[] fpTags = null, string connectionName = "floatdb", string database = "floatdb", string collection = "products", int limit = -1, int offset = 0)
		{
			try
			{
				// Creates a MongoClient and MongoDatabase
				IMongoClient imongoClient = DBConnectionManager.GetMongoDBConnection(connectionName);
				IMongoDatabase floatdb = imongoClient.GetDatabase(database);

				// Creates an object of MongoCollection conforming to model class FloatingPointFinal
				IMongoCollection<FloatingPointProductFinal> floatingPointsCollection = floatdb.GetCollection<FloatingPointProductFinal>(collection);

				// Creating Filter for Date Range Greater than Equal to Start Date and Less than End Date
				var builder = Builders<FloatingPointProductFinal>.Filter;

				var fpTagNameFilter = builder.Eq("IsAvailable", true);

				if (!string.IsNullOrWhiteSpace(fpTag))
					fpTagNameFilter = fpTagNameFilter & builder.Eq("FPTag", fpTag);

				if (fpTags != null && fpTags.Length > 0)
					fpTagNameFilter = fpTagNameFilter & builder.In("FPTag", fpTags);
				// DateTime objects time1 and time2 are used to calculate time taken to retrieve records between 
				List<FloatingPointProductFinal> list;
				DateTime time1 = DateTime.Now;

				// Retrieving records, if no/invalid limit is specified then all records are retrieved otherwise records as per specified limit and offset are retrieved
				if (limit < 0)
					list = floatingPointsCollection.Find(fpTagNameFilter).Project<FloatingPointProductFinal>(Builders<FloatingPointProductFinal>.Projection.Include("FPTag").Include("Name").Include("CreatedOn")).ToList();
				else
					list = floatingPointsCollection.Find(fpTagNameFilter).Project<FloatingPointProductFinal>(Builders<FloatingPointProductFinal>.Projection.Include("FPTag").Include("Name").Include("CreatedOn")).Skip(offset).Limit(limit).ToList();

				DateTime time2 = DateTime.Now;
				TimeSpan timeDifference = time2.Subtract(time1);
				logger.Info("Time taken to retrieve " + list.Count + " records for FPTag=" + fpTag + " = " + (timeDifference.Hours == 0 ? "" : timeDifference.Hours + " hours ") + (timeDifference.Minutes == 0 ? "" : timeDifference.Minutes + " minutes ") + (timeDifference.Seconds == 0 ? "" : timeDifference.Seconds + "seconds") + (timeDifference.Milliseconds == 0 ? "" : timeDifference.Milliseconds + " milliseconds"));
				return list;
			}
			catch (Exception e)
			{
				logger.Error(e, "Error Retrieving Records from FloatingPoint for FPTag " + fpTag);
				Console.WriteLine("Error Retrieving Records from FloatingPointfor FPTag " + fpTag);
				Console.WriteLine(e.Message + "\n" + e.StackTrace);
			}
			return null;
		}


		// MySQL MarketIntelligence //

		public static int ExecuteMySqlInsertQuery(string query, string connectionName = "marketIntelligence")
		{
			MySqlConnection mysqlConnectionObj = null;
			try
			{
				mysqlConnectionObj = DBConnectionManager.GetMySqlConnection(connectionName);
				if (mysqlConnectionObj == null)
					throw (new Exception());
			}
			catch (Exception e)
			{
				Console.WriteLine("Unable to create MySQL Connection for query:" + query + "\nError:" + e.Message + "\n" + e.StackTrace);
				logger.Error("Unable to create MySQL Connection for query:" + query + "\nError:" + e.Message + "\n" + e.StackTrace);
				return 0;
			}


			try
			{
				mysqlConnectionObj.Open();
			}
			catch (Exception e)
			{
				Console.WriteLine("Unable to open MySQL Connection for query:" + query + "\nError:" + e.Message + "\n" + e.StackTrace);
				logger.Error("Unable to open MySQL Connection for query:" + query + "\nError:" + e.Message + "\n" + e.StackTrace);
				return 0;
			}
			//Create Command

			MySqlCommand command = new MySqlCommand(query, mysqlConnectionObj);

			//Create a data reader and Execute the command
			int numOfRecords = 0;
			try
			{
				numOfRecords = command.ExecuteNonQuery();
			}
			catch (Exception e)
			{
				Console.WriteLine("Unable to execute SQL Query:" + query + "\nError:" + e.Message + "\n" + e.StackTrace);
				logger.Error("Unable to execute SQL Query:" + query + "\nError:" + e.Message + "\n" + e.StackTrace);
				return 0;
			}
			mysqlConnectionObj.Close();

			return numOfRecords;

		}

		public static List<string> RetrieveUnexpiredFPTagsFromFloatingPoints(string connectionName = "floatdb", string database = "floatdb", string collection = "floatingpoints", int limit = -1, int offset = 0)
		{
			try
			{
				// Creates a MongoClient and MongoDatabase
				IMongoClient imongoClient = DBConnectionManager.GetMongoDBConnection(connectionName);
				IMongoDatabase floatdb = imongoClient.GetDatabase(database);


				// Creates an object of MongoCollection conforming to model class FloatingPointFinal
				IMongoCollection<FloatingPointFinal> floatingPointsCollection = floatdb.GetCollection<FloatingPointFinal>(collection);

				// Creating Filter for Date Range Greater than Equal to Start Date and Less than End Date
				var builder = Builders<FloatingPointFinal>.Filter;
				var dateFilter = builder.And(builder.Eq("IsStoreFront", true), builder.Ne("PaymentLevel", -1));
				//var dateFilter = builder.Gte("CreatedOn", startDate) & builder.Lt("CreatedOn", endDate) & builder.Eq("IsStoreFront", true);

				// DateTime objects time1 and time2 are used to calculate time taken to retrieve records between 
				List<FloatingPointFinal> list;

				// Retrieving records, if no/invalid limit is specified then all records are retrieved otherwise records as per specified limit and offset are retrieved
				if (limit < 0)
					list = floatingPointsCollection.Find(dateFilter).Project<FloatingPointFinal>(Builders<FloatingPointFinal>.Projection.Include("Tag")).ToList();
				else
					list = floatingPointsCollection.Find(dateFilter).Skip(offset).Limit(limit).ToList();


				if (list == null || list.Count == 0)
					return null;

				return list.Select(x => x.Tag).ToList<string>();
			}
			catch (Exception e)
			{
				logger.Error(e, "Error Retrieving Records from FloatingPoint");
				Console.WriteLine("Error Retrieving Records from FloatingPoint");
				Console.WriteLine(e.Message + "\n" + e.StackTrace);
			}
			return null;
		}


		public static List<DateTime> GetLastXFetchedOnDates(string fpTag = "", int limit = 2)
		{
			string query1 = "select distinct fetchedOn from organicKeywordResearch order by fetchedOn desc";

			if (!string.IsNullOrWhiteSpace(fpTag))
				query1 = "select distinct fetchedOn from organicKeywordResearch where fpTag='" + fpTag.ToUpper() + "' order by fetchedOn desc";

			if (limit > 0)
				query1 = query1 + " limit " + limit;


			MySqlConnection mysqlConnectionObj = GetMySqlConnection();

			if (mysqlConnectionObj == null)
				return null;

			MySqlCommand command1 = new MySqlCommand(query1, mysqlConnectionObj);
			//Create a data reader and Execute the command
			MySqlDataReader mysqlDataReaderObj = null;
			try
			{
				mysqlDataReaderObj = command1.ExecuteReader();
			}
			catch (Exception e)
			{
				Console.WriteLine("Unable to execute SQL Query:" + query1 + "\nError:" + e.Message + "\n" + e.StackTrace);
				logger.Error("Unable to execute SQL Query:" + query1 + "\nError:" + e.Message + "\n" + e.StackTrace);
				if (mysqlDataReaderObj?.IsClosed == false)
					mysqlDataReaderObj.Close();

				mysqlConnectionObj?.CloseAsync();
				return null;
			}
			if (mysqlDataReaderObj == null || !mysqlDataReaderObj.HasRows)
			{
				logger.Warn("No FetchedOn Found in Database");
				if (mysqlDataReaderObj?.IsClosed == false)
					mysqlDataReaderObj.Close();

				mysqlConnectionObj?.CloseAsync();
				return null;
			}

			List<DateTime> fetchedOnList = new List<DateTime>();
			try
			{
				while (mysqlDataReaderObj.Read())
				{
					DateTime dt = mysqlDataReaderObj.GetDateTime(0);
					fetchedOnList.Add(dt);
				}
			}
			catch (Exception e)
			{
				logger.Error(e, "Unable to read last date");
				if (mysqlDataReaderObj?.IsClosed == false)
					mysqlDataReaderObj.Close();

				mysqlConnectionObj?.CloseAsync();
				return null;
			}
			if (mysqlDataReaderObj?.IsClosed == false)
				mysqlDataReaderObj.Close();

			mysqlConnectionObj?.CloseAsync();

			if (fetchedOnList == null || fetchedOnList.Count == 0)
				return null;

			return fetchedOnList;
		}
		public static MySqlConnection GetMySqlConnection(string connectionName = "marketIntelligence")
		{
			MySqlConnection mysqlConnectionObj = null;
			try
			{
				mysqlConnectionObj = DBConnectionManager.GetMySqlConnection(connectionName);
				if (mysqlConnectionObj == null)
					throw (new Exception());
			}
			catch (Exception e)
			{
				Console.WriteLine("Unable to create MySQL Connection" + "\nError:" + e.Message + "\n" + e.StackTrace);
				logger.Error("Unable to create MySQL Connection" + "\nError:" + e.Message + "\n" + e.StackTrace);
				return null;
			}


			try
			{
				mysqlConnectionObj.Open();
			}
			catch (Exception e)
			{
				Console.WriteLine("Unable to open MySQL Connection" + "\nError:" + e.Message + "\n" + e.StackTrace);
				logger.Error("Unable to open MySQL Connection" + "\nError:" + e.Message + "\n" + e.StackTrace);
				return null;
			}
			return mysqlConnectionObj;
		}

		public static Dictionary<string, int> GetLatestKeywordAndRankForFp(string fpTag)
		{
			List<DateTime> fetchedOnList = GetLastXFetchedOnDates(fpTag: fpTag, limit: 1);
			if (fetchedOnList == null || fetchedOnList.Count == 0)
				return null;

			return GetKeywordAndRankForFpAndDate(fpTag, fetchedOnList[0]);
		}

		public static Dictionary<string, int> GetKeywordAndRankForFpAndDate(string fpTag, DateTime dt)
		{
			MySqlConnection mysqlConnectionObj = GetMySqlConnection();

			if (mysqlConnectionObj == null)
				return null;

			string query = "select keyword,currentPosition from organicKeywordResearch where fpTag='" + fpTag + "' AND fetchedOn='" + dt.ToString("yyyy-MM-dd") + "' group by keyword order by currentPosition";

			MySqlCommand command = new MySqlCommand(query, mysqlConnectionObj);

			MySqlDataReader mysqlDataReaderObj = null;
			try
			{
				mysqlDataReaderObj = command.ExecuteReader();
			}
			catch (Exception e)
			{
				Console.WriteLine("Unable to execute SQL Query:" + query + "\nError:" + e.Message + "\n" + e.StackTrace);
				logger.Error("Unable to execute SQL Query:" + query + "\nError:" + e.Message + "\n" + e.StackTrace);
				if (mysqlDataReaderObj?.IsClosed == false)
					mysqlDataReaderObj.Close();

				mysqlConnectionObj?.CloseAsync();
				return null;
			}

			if (mysqlDataReaderObj == null || !mysqlDataReaderObj.HasRows)
			{
				logger.Warn("FPTag:" + fpTag + " No Records found in database for date " + dt);
				if (mysqlDataReaderObj?.IsClosed == false)
					mysqlDataReaderObj.Close();

				mysqlConnectionObj?.CloseAsync();
				return null;
			}

			Dictionary<string, int> keywordRank = new Dictionary<string, int>();
			try
			{
				while (mysqlDataReaderObj.Read())
				{
					string keyword = mysqlDataReaderObj.GetString(0);
					int rank = mysqlDataReaderObj.GetInt32(1);
					keywordRank[keyword] = rank;
				}
			}
			catch (Exception e)
			{
				logger.Error(e, "Unable to read last date");
				if (mysqlDataReaderObj?.IsClosed == false)
					mysqlDataReaderObj.Close();

				mysqlConnectionObj?.CloseAsync();
				return null;
			}

			if (mysqlDataReaderObj?.IsClosed == false)
				mysqlDataReaderObj.Close();

			mysqlConnectionObj?.CloseAsync();

			if (keywordRank == null || keywordRank.Count == 0)
				return null;

			return keywordRank;
		}


		public static List<FPMarketTrendSummaryRecord> GetProductsAndKeywordRankingsForLeastRecentlyUpdatedFPs(int limit = -1)
		{
			MySqlConnection mysqlConnectionObj = GetMySqlConnection();

			if (mysqlConnectionObj == null)
				return null;

			string query = "select fp_tag,fp_createdOn,fp_category,fp_keywordBag,fp_productList,fp_keyword_page1,fp_keyword_page2,fp_keyword_pageOther,fp_seed_keywords,fp_suggestedKeywords,last_update_date from organicKeywordResearchSummary order by last_update_date,fp_tag";

			if (limit >= 0)
				query += " limit " + limit;

			MySqlCommand command = new MySqlCommand(query, mysqlConnectionObj);

			MySqlDataReader mysqlDataReaderObj = null;
			try
			{
				mysqlDataReaderObj = command.ExecuteReader();
			}
			catch (Exception e)
			{
				Console.WriteLine("Unable to execute SQL Query:" + query + "\nError:" + e.Message + "\n" + e.StackTrace);
				logger.Error("Unable to execute SQL Query:" + query + "\nError:" + e.Message + "\n" + e.StackTrace);
				if (mysqlDataReaderObj?.IsClosed == false)
					mysqlDataReaderObj.Close();

				mysqlConnectionObj?.CloseAsync();
				return null;
			}

			if (mysqlDataReaderObj == null || !mysqlDataReaderObj.HasRows)
			{
				logger.Warn("No Records found in marketSummary database ");
				if (mysqlDataReaderObj?.IsClosed == false)
					mysqlDataReaderObj.Close();

				mysqlConnectionObj?.CloseAsync();
				return null;
			}

			List<FPMarketTrendSummaryRecord> fpMarketSummaryList = new List<FPMarketTrendSummaryRecord>();
			try
			{
				if (mysqlDataReaderObj.HasRows)
					while (mysqlDataReaderObj.Read())
					{
						try
						{
							FPMarketTrendSummaryRecord fpMarketSummaryRecord = new FPMarketTrendSummaryRecord(mysqlDataReaderObj.GetString(0), mysqlDataReaderObj.GetDateTime(1), mysqlDataReaderObj.GetString(2), mysqlDataReaderObj.GetString(3), mysqlDataReaderObj.GetString(4), mysqlDataReaderObj.GetString(5), mysqlDataReaderObj.GetString(6), mysqlDataReaderObj.GetString(7), mysqlDataReaderObj.GetString(8), mysqlDataReaderObj.GetString(9));
							fpMarketSummaryRecord.LastUpdatedDate = mysqlDataReaderObj.GetDateTime(10);

							fpMarketSummaryList.Add(fpMarketSummaryRecord);
						}
						catch (Exception e)
						{
							Console.WriteLine(e);
						}
					}
			}
			catch (Exception e)
			{
				logger.Error(e, "Unable to read last date");
				if (mysqlDataReaderObj?.IsClosed == false)
					mysqlDataReaderObj.Close();

				mysqlConnectionObj?.CloseAsync();
				return null;
			}

			if (mysqlDataReaderObj?.IsClosed == false)
				mysqlDataReaderObj.Close();

			mysqlConnectionObj?.CloseAsync();

			if (fpMarketSummaryList == null || fpMarketSummaryList.Count == 0)
				return null;

			return fpMarketSummaryList;
		}
	}
}
