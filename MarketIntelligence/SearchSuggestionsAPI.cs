﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Xml.Linq;

namespace MarketIntelligence
{
	public class SearchSuggestionsApi
	{
		private const string SuggestSearchUrl = "http://www.google.com/complete/search?output=toolbar&q={0}&hl=en";

		public List<GoogleSuggestion> GetSearchSuggestions(string query)
		{
			if (String.IsNullOrWhiteSpace(query))
				throw new ArgumentException("Argument cannot be null or empty!", "query");

			string result = String.Empty;
			//Console.WriteLine(query);
			using (HttpClient client = new HttpClient())
				result = client.GetStringAsync(string.Format(SuggestSearchUrl, query)).Result;

			XDocument doc = XDocument.Parse(result);

			var suggestions = from suggestion in doc.Descendants("CompleteSuggestion")
							  select new GoogleSuggestion
							  {
								  Phrase = suggestion.Element("suggestion").Attribute("data").Value
							  };

			return suggestions.ToList();
		}

		public List<GoogleSuggestion> GetSearchResults(string query)
		{
			HashSet<GoogleSuggestion> list = new HashSet<GoogleSuggestion>();
			var v = GetSearchSuggestions(query);
			foreach (GoogleSuggestion x in v)
				list.Add(x);

			for (char c = 'a'; c <= 'z'; c++)
			{
				var tempList = GetSearchSuggestions(c + " " + query);
				foreach (GoogleSuggestion x in tempList)
					list.Add(x);

				tempList = GetSearchSuggestions(query + " " + c);
				foreach (GoogleSuggestion x in tempList)
					list.Add(x);
			}
			for (char c = '0'; c <= '9'; c++)
			{
				var tempList = GetSearchSuggestions(c + " " + query);
				foreach (GoogleSuggestion x in tempList)
					list.Add(x);

				tempList = GetSearchSuggestions(query + " " + c);
				foreach (GoogleSuggestion x in tempList)
					list.Add(x);
			}
			return list.ToList<GoogleSuggestion>();
		}
	}

	public class GoogleSuggestion
	{
		public string Phrase
		{
			get; set;
		}

		public override string ToString()
		{
			return this.Phrase;
		}
	}
}