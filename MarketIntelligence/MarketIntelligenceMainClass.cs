﻿using MarketIntelligence.Database;
using MarketIntelligence.Models.SemRush;
using MongoDB.Bson.Serialization.Conventions;
using MovingFloats.Server.Model;
using Nager.PublicSuffix;
using NLog;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace MarketIntelligence
{

	public static class MarketIntelligenceMainClass
	{
		const string SemRushApi = "http://api.semrush.com/?key=45f690d9f6a7f575c3519d8cabceaddd";

		private static readonly Logger logger = LogManager.GetCurrentClassLogger();


		static DomainParser domainParser = new DomainParser(new WebTldRuleProvider());


		public static void Main2(string[] args)
		{

			//string[] fpTags = { "50000VOLTS", "68MHOLIDAYS", "77OFFICESOLUTIONSINTERIORDESIGNINDIA", "ABDULREFRIGERATIONCENTER", "ADBOOKINGINDIA", "ADVOCATERAVINDERTYAGI", "AGARWALCROCKERYHOUSE", "AGRAMITHAIWALA", "ALLTECHZSOLUTIONSPVTLTD", "AMAZINGART", "ANANTPLAZA", "ANAYSGALLERY", "APJCABS", "ARIHANTOILCHEMICALS", "ASHEI", "ASHRAYMEDICALCENTRE", "ASQUAREMARKETINGCBE", "ASTROLOGYDELHI", "AVIGHNAPOWERSOLUTION", "AYUSHISERVICES", "BAAROQFANS", "BABYGARMENTS", "BALAJITEA", "BALLOONSDECORATORSVIZAG", "BESTDRONEDEALERSININDIA", "BEYONDBOUNDARIESTOURISMCHENNAI", "BOXPOWERENTERPRISES", "BUVANAGIRIHANDLOOMS", "CADETSACADEMY", "CANONHYDERABAD", "CARPOOLING", "CATERINGSERVICESDELHI", "CELLPETFOODONLINE", "CHARUNOPTIC", "CHENNAISAMIRTAIIHM1", "CHRISTIANBRIDALGOWNS", "COMECOMEINDIA", "CORPUSGYMEQUIP", "CREEZ", "CUPCAKEHOUSE", "DELHIDETECTIVES", "DETECTIVES", "DHIYAACARS", "DHVE", "DIABETESHYPERTENSION", "DIGITECHELECTRONICSINDIA", "DILKASHFASHIONHOUSE", "DISCOUNTBAZAAR", "DREAMKITCHENINTERIOR", "DWARKADHISHINDUSTRIES", "EDUCATIONDIARY", "ENERGYBEVERAGESPVTLTD", "EXIDEHYDERABAD", "EXPRESSLINKERS", "EYEMAXOPTICIAN", "FASHIONPASSION", "FIRSTMAN", "FREEDOMAHMEDABAD", "FREEZEAIRMARKETING", "FURNITUREZONE", "FUSION9INORBIT", "GANGAENTERPRISES41", "GENIUSINSPECTION", "GKPETCARECLINIC", "GOLDENHICARE", "GRANNYSINN", "HAIRTRIX", "HARBALLIFEDELHI", "HATSOFFACCESSORIES", "HBSERVICES", "HEALTHKARTDELHI", "HERBALIFEPRODUCTS", "HERBALWELLNESSCENTRE", "HIGHFASHIONFASHIONAPPARELNEWDELHI", "HIJAB", "HIVAIDSCLINIC", "HMTRAVELS", "HOMEMART", "HOUSEOFTREASURES", "HUNT4CAREERS", "HYDROSEALSINDIA", "IDEVTEST5", "IMI", "IMMACULATEHAIRCLINIC", "INCREDIBLESOLUTION", "INDIANDANCEACADEMY", "IQRAMEDIAGRAPHICS", "ITOOLSMOBILESERVICECENTER", "JAGADISHHEARINGAIDS", "JAICHITTRAINC", "JAINMARKETINGAHMEDABAD", "JAYPHOMESTAY", "JIMMYPUPPYKENNEL", "JJENTERPRISES", "JMSINDUSTRIES", "KALAPURNAM", "KIKABONI", "KODAIKKANALVILLA", "KRISHNAHOMENEEDS", "KUKREJABROTHERS", "KUNWARBROS", "LANDSININFRA", "LAPTOPSERVICEGBS2", "LEADER", "LEGGINGSMANUFACTURER", "LICAJAYGUPTA", "LIFECARESYSTEMS", "LINGERIESTORE", "MAACKAROLBAGH", "MADHAVIKHANDWALA", "MADHAVWEBHOSTING", "MADMUSICALS", "MAHARAJRAJASHARMA", "MAHENDERSINGH", "MAKEUPARTISTOTHERRETAILINDIA", "MALINSBEAUTYCOURSE", "MANINAGARAHMEDABAD", "MANISHMEHNDIART2", "MANTRICABSBENGALURU", "MASCOTMACHINES", "MASSCOMMUNICATIONINDELHI", "MEENAKSHI", "MIMONLINE", "MIRACLESASTROLOGY", "MULIALEGACY", "MUNIMAUTO", "NABANNOHYD", "NANIUSEDCARS", "NETLABSTRAINING", "NKENTERPRISESDELHI", "OCCASIONORGANIZER2", "OLIVECONSULATANTS", "OLIVESWEETS", "OMSURYASPORTS", "ONLINEWINES", "OTTOCLOTHING", "OVIYAFASHIONS", "PAINTINGSFRAMES", "PALAMURUGRILL", "PANCHKARMAAGRA", "PATELBROTHERSGENERALSERVICESKOLKATA", "PAYOTM", "PEARLCONSTRUCTIONS", "PERFECTCAREER", "PITSTOP", "PLANETFURNITURES", "PRADEEPCHAMARIATHEPHOTOGRAPHER", "PROJECTORSERVICECENTER", "QUALITYROOFS", "RAHULDUTTAPHOTOGRAPHY", "RAJBRIDALMAKEUP", "RAKSHANEQUIPMENTS", "RAYBANEXCLUSIVESTORE", "RAYNA", "REIKIBLESSYOUFOUNDATION1", "RESORTSINJIMCORBETT", "RISHAFUTURETECHPVTLTD", "RKENTERPRISESGENERALSERVICESHYDERABAD", "ROCKCASTLERESTAURANT", "SAHILCHAWLA", "SAMUIHEALTHSHOP", "SANCHITAPPLIANCES", "SANUZBEAUTYSTUDIO", "SARAHFURNITURE", "SARATH", "SEASHELLSENCLAVE", "SEKAR", "SHIVFIREENGINEERSGENERALSERVICESINDIA", "SHREEFIREPACKSAFETYPVTLTD", "SHREELUCKMIRAAMASSOCIATES643", "SHRISWAMINARAYAN", "SIKKIMMANIPALUNIVERSITY", "SKINMAYA", "SKYLIGHTACADEMY", "SKYVOICEMOBILINKSERVICESPVTLTD", "SLOTTEDRACK", "SMARTINTERIORS", "SOUTHINDIASHOPPINGMALL", "SPAARKMEDIA", "SPAREMART", "SPYZONE", "SREETECHINTERIOR", "SREETECHINTERIOR6", "SRISAICOMMUNICATIONS35", "STEELMODULARKITCHEN", "SUNILTOURTRAVELS4", "SUNMACAUTOMOTIVE", "SUPERLOCKCABLETIES", "TAG", "TANJOREPAINTINGS", "TAPOVANAORGANICFARMS", "TENSILESTRUCTURES", "THEBRAINSEEDER", "THECHENNAIFURNITURE", "THECUTFASHIONBOUTIQUE", "TIMBEWINDOWSPVTLTD", "TOPCINEBRIDALMAKEUP", "TRAVELERSHOME", "TUSHARMARRIAGEBUREAU", "UNISTARELECTRONICS", "VAJRACABS", "VASANTMOTORS", "VENDINGMACHINEDELHI", "VIBHAINTERNATIONAL", "VIEWTECH", "VIJAYIMAGINGPRODUCTS", "VIMALINDUSTRIALSYSTEM", "VIRTUOSOACADEMY", "VISHESHHOMESTYLE", "VISHWIMPEX", "WEIGHINGSCALES", "WEIGHTLOSSHYDERABAD", "WHEELDEAL", "WOODDESIGNES", "YADAVPACKERSANDMOVERS", "YUGAMODULARKITCHENS", "ZAITOONRESTAURANT" };
			//string[] fpTags = { "ARCHANAGUESTHOUSE", "GOPIROOFREATAURENT", "RAVISROSE", "PUSHPAGUESTHOUSE", "ROCKYGUESTHOUSE", "GOPIGUESTHOUSE", "VICKYSGUESTHOUSE", "SURESHRESTAURANT", "OMSHIVAGUESTHOUSE", "SHIVAMOON", "GOUTHAMIGUESTHOUSE", "MAYURAMHOMESTAY", "GOLDENGARDENHAMPI", "RAVISROSEROOFTOPRESTAURANT", "GIRIHAMPIGMAILCOM", "SWAGATHHOMESTAY", "SHIVAGEMSJEWELRY", "KKAROMAS", "DESERTLEATHERARTS", "LEATHERSLEEPERSHOP", "THEROCKSHOP", "DONTWORRYBEHAMPI", "RAJUGUESTHOUSE", "SIAPLAZAGUESTHOUSE", "SUNNYGUESTHOUSE", "FUNKYMONKEY", "POONAMARTGALLERY", "SOUVENIRSHOP", "BESTANTIQUEITEM", "INDIANMUSICALSHOP", "HEMAGUESTHOUSE", "NARGILACLOTHESSHOP", "SANIYARESTAURANT", "SASIRESTAURANT", "RAVITEJAGUESTHOUSE", "BIKESFORRENT" };

			//{ "ASTRALPROJECTSCORPORATION", "BABASERVICES", "BHARATHNAUKARI", "CHIRANJIT", "CHLORODOTSTECHNOLOGIESPVTLTD", "CHOICE4U", "CORNERLINES", "COSMICKITCHENS", "DESIGNSHUB", "DEVEN", "DIGITALMARKETINGOTHERKHAMMAM", "DROPYOU", "FINDMYGURU", "FLASHLIGHTMODELS", "GANESHMANGALAFOUNDATION", "GAURAVKAVITALEKH", "GOODLUCKGENERALNAGPUR", "GURJAR", "GVRMARKETING", "HEMENDRAMASHRU", "HKPROPERTIES", "INVESTCONS", "JPLARTPALACE", "JSINFORMATICS", "KARANBANGERA", "KUBERPHARMA", "LEARNINGBLOGSKOLKATA", "LOTUSADVERTISINGAGENCY", "LOVURFOOD", "MANGALAMBLOGSPONDICHERRT", "MARKETINGHOMEAPPLIANCESKAPURTHALA", "MEDIATECKBOARDS", "MEGHABOOKS", "MOBILEOTHERLIMKHEDA", "MUMMYHELP", "MYNAGPURCAB", "NABEEL", "NICHESOFTWARESOLUTIONSINC", "PADMAKARJOSHI", "PICKER", "PIYARA", "PULLARAO", "PUNEDENTALSTUDIOAUNDH", "RAAJ", "RAGAMAJMUDAR", "RAJCOMPUTERSSERVICESPVTLTD", "RAJIMPEX", "RIDEINDIATOURCOM", "ROYALRAJWADARESTAURANT", "SANTOSHCHATURVEDI", "SHARMACOMMUNOCATION", "SRISAIRAMGRAPHICS", "SWADESHRANJANDASH", "SWASTHYALIFECARE", "THECOMPUTERWORLD", "UNIQUEMULTISERVICES", "VASANTIVILAS", "VISESHELECTRONICS" };
			/*
			List<FloatingPointFinal> fpTagList = new List<FloatingPointFinal>();
			Parallel.ForEach(fpTags, fpTag =>
			 {
				 FloatingPointFinal fpObj = DBHandler.RetrieveRecordFromFloatingPointsUsingFPTag(fpTag);
				 //Console.WriteLine(IsDomainOrUrl(fpObj?.RootAliasUri) + "|" + fpTag + "\t" + fpObj?.RootAliasUri ?? "ERROR");
				 //if (fpObj != null && !string.IsNullOrWhiteSpace(fpObj.RootAliasUri))
				 fpTagList.Add(fpObj);
			 });
			Process(fpTagList);*/

			var v = DBHandler.RetrieveNumberOfRecordsFromFloatingPointsInDateRange(new DateTime(2016, 03, 01), new DateTime(2017, 04, 01));
			Console.WriteLine(v);
			//Console.WriteLine(v.Exists(x => x.Tag == "VASANTMOTORS"));
			//Console.WriteLine(v.Exists(x => x.Tag == "HIJAB"));

			Console.ReadLine();
		}
		static void Main3(string[] args)
		{

			ConventionRegistry.Register(typeof(IgnoreExtraElementsConvention).Name, new ConventionPack { new IgnoreExtraElementsConvention(ignoreExtraElements: true) }, t => true);

			//int[] month = { 0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

			for (int i = 2012; i <= 2017; i++)
			{
				//if (i % 4 == 0)
				//	month[2] = 29;
				//else
				//	month[2] = 28;
				for (int j = 1; j <= 12; j++)
				{
					if (i == 2017 && j > 3)
						continue;

					//for (int k = 1; k <= month[j]; k++)
					//{
					DateTime startDate = new DateTime(i, j, 1, 0, 0, 0, DateTimeKind.Utc);
					DateTime endDate = startDate.AddMonths(1);
					Console.WriteLine("DATERANGE:: " + startDate + "-->" + endDate);
					List<FloatingPointFinal> fpList = DBHandler.RetrieveRecordsFromFloatingPointsInDateRange(startDate, endDate);
					if (fpList != null && fpList.Count > 0)
					{
						Console.WriteLine("Number of FPs:" + fpList.Count);
						try
						{
							Process(fpList);
						}
						catch (Exception e)
						{
							Console.WriteLine(e);
							i = 3000;
							j = 200;
						}
					}
					Console.ReadLine();

					//}
					// Console.WriteLine("NO RECORDS FOUND IN:: " + startDate + "-->" + endDate);
					//Console.WriteLine("FINISHED DATERANGE:: " + startDate + "-->" + endDate);
					//Console.Clear();
				}
			}
			Console.WriteLine("FINISHED");
			Console.ReadLine();
			Console.ReadLine();
			Console.ReadLine();
			Console.ReadLine();
			Console.ReadLine();
			Console.ReadLine();

		}

		public static void Process(List<FloatingPointFinal> fpList)
		{
			ConcurrentBag<OrganicSearchModel> organicSearchResultBag = new ConcurrentBag<OrganicSearchModel>();

			ParallelOptions po = new ParallelOptions();
			po.MaxDegreeOfParallelism = 20;
			Parallel.ForEach(fpList, po, fp =>
			  {
				  string domainOrUrl = "";
				  if (string.IsNullOrWhiteSpace(fp.Tag))
					  return;

				  if (string.IsNullOrWhiteSpace(fp.RootAliasUri))
				  {
					  domainOrUrl = "https://" + fp.Tag.ToLower() + ".nowfloats.com/";
				  }
				  else
				  {
					  domainOrUrl = fp.RootAliasUri.ToLower();
					  if (!(domainOrUrl.StartsWith("http://") || domainOrUrl.StartsWith("https://")))
						  domainOrUrl = "http://" + domainOrUrl;
				  }

				  List<OrganicSearchModel> listOrganicSearchResultsUSDatabase = GetOrganicSearchInsights(fp.Tag, domainOrUrl, database: DatabaseCode.US);
				  List<OrganicSearchModel> listOrganicSearchResultsINDatabase = GetOrganicSearchInsights(fp.Tag, domainOrUrl, database: DatabaseCode.IN);

				  if (listOrganicSearchResultsUSDatabase != null && listOrganicSearchResultsUSDatabase.Count > 0)
				  {
					  foreach (OrganicSearchModel organicSearchKeyword in listOrganicSearchResultsUSDatabase)
						  organicSearchResultBag.Add(organicSearchKeyword);
				  }
				  if (listOrganicSearchResultsINDatabase != null && listOrganicSearchResultsINDatabase.Count > 0)
				  {
					  foreach (OrganicSearchModel organicSearchKeyword in listOrganicSearchResultsINDatabase)
						  organicSearchResultBag.Add(organicSearchKeyword);
				  }

			  });

			if (organicSearchResultBag != null && organicSearchResultBag.Count > 0)
			{
				int n = InsertOrganicSearchResultsIntoDatabase(organicSearchResultBag.ToList());
				Console.WriteLine("Records Inserted:" + n);
			}
		}

		private static int InsertOrganicSearchResultsIntoDatabase(List<OrganicSearchModel> listOrganicSearchResults)
		{
			if (listOrganicSearchResults == null || listOrganicSearchResults.Count == 0)
				return -1;

			string query;

			int offset = 0;
			int limit = 500;
			int numOfRecordsInserted = 0;
			while (offset < listOrganicSearchResults.Count)
			{
				query = "INSERT IGNORE INTO organicKeywordResearch(source, fpTag, urlType, url, databaseCode, keyword, currentPosition, searchVolume, costPerClick, competition, trafficPercentage, trafficCostPercentage, numberOfResults, trends, previousPosition, positionDifference, fetchedOn) VALUES";


				int endPoint = offset + limit;
				if (endPoint > listOrganicSearchResults.Count)
					endPoint = listOrganicSearchResults.Count;

				for (int i = offset; i < endPoint; i++)
					query += "(" + listOrganicSearchResults[i] + "),";

				offset = endPoint;
				if (query.EndsWith(","))
					query = query.Substring(0, query.Length - 1);

				numOfRecordsInserted += DBHandler.ExecuteMySqlInsertQuery(query);

			}
			return numOfRecordsInserted;


		}

		static List<OrganicSearchModel> GetOrganicSearchInsights(string fpTag, string domainOrUrl, int displayLimit = 250, string database = DatabaseCode.US)
		{
			List<OrganicSearchModel> organicSearchList = new List<OrganicSearchModel>();

			string semRushApiForOrganicSearch = SemRushApi;
			string urlType = "";
			int domainOrUrlCode = IsDomainOrUrl(domainOrUrl);

			if (domainOrUrlCode == 0)
			{
				Uri ur = null;
				try
				{
					ur = new Uri(domainOrUrl);
				}
				catch (Exception e)
				{
					Console.WriteLine("Unable to parse domain url " + fpTag + "\t" + domainOrUrl + e);
					logger.Error(e, "Unable to parse domain url " + fpTag + "\t" + domainOrUrl);
				}

				semRushApiForOrganicSearch += "&type=domain_organic";
				semRushApiForOrganicSearch += "&export_columns=Ph,Po,Nq,Cp,Co,Tr,Tc,Nr,Td,Pp,Pd,Ur";
				semRushApiForOrganicSearch += "&domain=" + ur.DnsSafeHost;
				urlType = "DOMAIN";
			}
			else if (domainOrUrlCode == 1)
			{
				if (!string.IsNullOrWhiteSpace(domainOrUrl) && !domainOrUrl.EndsWith("/") && !domainOrUrl.Contains("?"))
					domainOrUrl = domainOrUrl + "/";

				semRushApiForOrganicSearch += "&type=url_organic";
				semRushApiForOrganicSearch += "&export_columns=Ph,Po,Nq,Cp,Co,Tr,Tc,Nr,Td";
				semRushApiForOrganicSearch += "&url=" + domainOrUrl;
				urlType = "URL";
			}
			else
			{
				Console.WriteLine("Invalid Domain Or URL " + fpTag + "\t" + domainOrUrl);
				logger.Error("Invalid Domain Or URL " + fpTag + "\t" + domainOrUrl);
				return null;
			}


			semRushApiForOrganicSearch += "&display_limit=" + displayLimit;
			semRushApiForOrganicSearch += "&database=" + database;

			Console.WriteLine(urlType + "|" + fpTag + "|" + domainOrUrl);
			logger.Info(urlType + "|" + fpTag + "|" + domainOrUrl);
			logger.Info(semRushApiForOrganicSearch);

			HttpWebRequest request = (HttpWebRequest) WebRequest.Create(semRushApiForOrganicSearch);
			try
			{
				request.Timeout = 1000000;
				WebResponse response = request.GetResponse();
				Encoding encoding = ASCIIEncoding.ASCII;
				string responseText = null;
				using (StreamReader reader = new System.IO.StreamReader(response.GetResponseStream(), encoding))
				{
					responseText = reader.ReadLine();
					if (responseText != null)
						responseText = reader.ReadLine();

					while (responseText != null)
					{
						//Console.WriteLine(domainOrUrl + "\t" + responseText);

						string[] param = responseText.Split(';');
						if (param.Length >= 9)
						{
							try
							{
								string keyword = param[0];
								if (keyword != null)
									keyword = keyword.Trim();

								int position = Int32.Parse(param[1]);
								int searchVolume = Int32.Parse(param[2]);
								double costPerClick = Double.Parse(param[3]);
								double competition = Double.Parse(param[4]);
								double trafficPercentage = Double.Parse(param[5]);
								double trafficCostPercentage = Double.Parse(param[6]);
								long numberOfResults = long.Parse(param[7]);
								string trends = param[8];
								if (trends != null)
									trends = trends.Trim();

								int previousPosition = position;
								int positionDifference = 0;
								string url = domainOrUrl;
								if (param.Length == 12)
								{
									previousPosition = Int32.Parse(param[9]);
									positionDifference = Int32.Parse(param[10]);
									url = param[11];
								}

								OrganicSearchModel organicSearchModelObj = new OrganicSearchModel("SEMRUSH", fpTag, urlType, url, database, keyword, position, searchVolume, costPerClick, competition, trafficPercentage, trafficCostPercentage, numberOfResults, trends, previousPosition, positionDifference, DateTime.UtcNow.Date);

								organicSearchList.Add(organicSearchModelObj);

							}
							catch (Exception e)
							{
								Console.WriteLine(e);
								foreach (string str in param)
									Console.Write(str + "\t");

								Console.WriteLine();
							}
						}
						else
						{
							foreach (string str in param)
								Console.Write(str + "\t");

							Console.WriteLine();
						}
						responseText = reader.ReadLine();
					}

				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				logger.Error(e);
				return null;
			}

			if (organicSearchList != null && organicSearchList.Count == 0)
				return null;

			return organicSearchList;
		}

		public static int IsDomainOrUrl(string domainOrUrl)
		{
			if (string.IsNullOrWhiteSpace(domainOrUrl) || domainOrUrl.Contains("stores.ezoneonline.in"))
				return -1;

			string host = "";

			if (!(domainOrUrl.StartsWith("http://") || domainOrUrl.StartsWith("https://")))
				domainOrUrl = "http://" + domainOrUrl;

			Uri myUri;
			DomainName domainName;


			try
			{
				myUri = new Uri(domainOrUrl);
				host = myUri.DnsSafeHost;

				if (string.IsNullOrWhiteSpace(host))
					return -1;

				if (!string.IsNullOrWhiteSpace(myUri.LocalPath) && myUri.LocalPath != "/")
					return 1;
				else if (!string.IsNullOrWhiteSpace(myUri.Query) && myUri.Query != "?")
					return 1;
				else if (host.EndsWith(".canadian.website"))
					return 1;
				else if (host.EndsWith(".co.com"))
					return 1;
				else if (host.EndsWith(".nowfloats.com"))
					return 1;
				else
				{
					if (!(host.EndsWith(".com") || host.EndsWith(".net") || host.EndsWith(".org") || host.EndsWith(".co.in") || host.EndsWith(".in")))
						return 1;
				}

			}
			catch (Exception e)
			{
				Console.WriteLine("ERROR while URI Parsing:::::" + domainOrUrl + "\t" + e.Message);
				logger.Error(e, "ERROR while URI Parsing " + domainOrUrl + "\t" + e.Message);
				return -1;
			}

			try
			{
				//Console.WriteLine("HOST:" + host);
				domainName = domainParser.Get(host);
				if (domainName == null)
				{
					Console.WriteLine("ERROR while Domain Parsing:::::" + domainOrUrl);
					logger.Error("ERROR while Domain Parsing " + domainOrUrl);
					return -1;
				}

				if (string.IsNullOrWhiteSpace(domainName.SubDomain) || domainName.SubDomain == "www")
					return 0;
				else
					return 1;
			}
			catch (Exception e)
			{
				Console.WriteLine("ERROR while Domain Parsing:::::" + domainOrUrl + "\t" + e.Message);
				logger.Error(e, "ERROR while Domain Parsing " + domainOrUrl + "\t" + e.Message);
				return -1;
			}
		}

		public static void StoreValidAndInvalidFPs()
		{
			List<FloatingPointFinal> fpList = DBHandler.RetrieveRecordsFromFloatingPointsInDateRange(new DateTime(2010, 11, 01), new DateTime(2016, 12, 01));

			System.Net.ServicePointManager.DefaultConnectionLimit = 10000;
			try
			{
				Directory.CreateDirectory("fpTestFolder");
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				return;
			}
			using (TextWriter all = TextWriter.Synchronized(new System.IO.StreamWriter(@"fpTestFolder\all" + DateTime.Now.ToString("ddMMyyyy HHmmss") + ".csv", false)))
			{
				using (TextWriter active = TextWriter.Synchronized(new System.IO.StreamWriter(@"fpTestFolder\active" + DateTime.Now.ToString("ddMMyyyy HHmmss") + ".csv", false)))
				{
					using (TextWriter inactive = TextWriter.Synchronized(new System.IO.StreamWriter(@"fpTestFolder\inactive" + DateTime.Now.ToString("ddMMyyyy HHmmss") + ".csv", false)))
					{
						Parallel.ForEach(fpList, fp =>
						{
							if (!string.IsNullOrWhiteSpace(fp.RootAliasUri))
							{
								//Console.WriteLine(fp.Tag + "\t" + fp.RootAliasUri);
								all.WriteLine(fp.Tag + "|" + fp.RootAliasUri + "|" + fp.CreatedOn);
								string iP;
								if (PingHost(fp.RootAliasUri, out iP))
									active.WriteLine(fp.Tag + "|" + fp.RootAliasUri + "|" + fp.CreatedOn + "|" + iP);
								else
									inactive.WriteLine(fp.Tag + "|" + fp.RootAliasUri + "|" + fp.CreatedOn);
							}

						});
					}
				}

			}

			Console.WriteLine("Finished Processing");

		}
		public static bool PingHost(string nameOrAddress, out string ipAddress)
		{
			string host = "";
			ipAddress = "0.0.0.0";
			nameOrAddress = nameOrAddress.ToLower();

			if (!nameOrAddress.StartsWith("http"))
				nameOrAddress = "http://" + nameOrAddress;

			try
			{
				Uri myUri = new Uri(nameOrAddress);
				host = myUri.Host;
			}
			catch (Exception e)
			{
				Console.WriteLine(nameOrAddress + "\t" + e.Message);
				host = nameOrAddress;
			}
			bool pingable = false;
			Ping pinger = new Ping();
			try
			{
				/*PingReply reply = pinger.Send(host, 100);
				pingable = reply.Status == IPStatus.Success;
				*/

				IPAddress[] addressList = Dns.GetHostAddresses(host);
				foreach (IPAddress address in addressList)
				{
					if (address.ToString() == "54.255.144.74")
					{
						ipAddress = "54.255.144.74";
						break;
					}
				}
				if (ipAddress == "0.0.0.0" && addressList.Length > 0)
					ipAddress = addressList[0].ToString();
			}
			catch (PingException p)
			{
#pragma warning disable JustCode_LiteralArgumentIsNotNamedDiagnostic // The used literal argument is not named
				if (!(p.ToString().Contains("No such host is known") || p.ToString().Contains("This is usually a temporary error during host name resolution and means that the local server did not receive a response from an authoritative server")))
#pragma warning restore JustCode_LiteralArgumentIsNotNamedDiagnostic // The used literal argument is not named
				{
					Console.WriteLine(nameOrAddress + "\t" + p);
				}
			}
			catch (System.Net.Sockets.SocketException e)
			{
				if (!e.ToString().Contains("No such host is known"))
					Console.WriteLine(e);
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
			}
			if (pingable == false)
			{
				HttpWebRequest hw = (HttpWebRequest) WebRequest.Create(nameOrAddress);
				hw.Method = "HEAD";
				try
				{
					HttpWebResponse hr = (HttpWebResponse) hw.GetResponse();
					//Console.WriteLine(nameOrAddress + "\tworks");
					if (hr.StatusCode == HttpStatusCode.OK)
					{
						pingable = true;
					}
					else
					{
						if (hr.StatusCode == HttpStatusCode.MethodNotAllowed)
							throw new WebException();
						else
							Console.WriteLine("CHECK THIS USING HTTP:\t" + nameOrAddress);
					}
				}
				catch (WebException)
				{
					try
					{
						hw.Method = "GET";
						HttpWebResponse hr = (HttpWebResponse) hw.GetResponse();
						if (hr.StatusCode == HttpStatusCode.OK)
							pingable = true;
					}
					catch (Exception e)
					{
						Console.WriteLine(e);
					}
				}
				catch (Exception e2)
				{
					Console.WriteLine(nameOrAddress + "\t" + e2.Message);
				}

			}
			return pingable;
		}


	}

	public static class DatabaseCode
	{
		public const string US = "us";
		public const string IN = "in";
		public const string Mobile = "us-mobile";
	}
}
