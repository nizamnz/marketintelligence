﻿using System;

namespace MarketIntelligence.Models
{
	public class FPMarketTrendSummaryRecord
	{

		public string FpTag { get; set; }
		public DateTime FpCreatedOn { get; set; } = DateTime.MinValue;
		public string FpCategory { get; set; }
		public string FpKeywordBag { get; set; } = "";
		public string FpProductList { get; set; } = "";
		public string FpKeywordPage1 { get; set; } = "";
		public string FpKeywordPage2 { get; set; } = "";
		public string FpKeywordPageOther { get; set; } = "";
		public string FpSeedKeywords { get; set; } = "";
		public string FpSuggestedKeywords { get; set; } = "";

		public DateTime LastUpdatedDate { get; set; } = DateTime.MinValue;

		public FPMarketTrendSummaryRecord()
		{
		}

		public FPMarketTrendSummaryRecord(
				string fpTag, DateTime fpCreatedOn, string fpCategory)
		{
			this.FpTag = fpTag;
			this.FpCreatedOn = fpCreatedOn;
			this.FpCategory = fpCategory;
		}

		public FPMarketTrendSummaryRecord(string fpKeywordPage1, string fpKeywordPage2, string fpKeywordPageOther)
		{
			this.FpKeywordPage1 = fpKeywordPage1;
			this.FpKeywordPage2 = fpKeywordPage2;
			this.FpKeywordPageOther = fpKeywordPageOther;
		}
		public FPMarketTrendSummaryRecord(string fpTag, DateTime fpCreatedOn, string fpCategory, string fpKeywordBag, string fpProductList, string fpKeywordPage1, string fpKeywordPage2, string fpKeywordPageOther, string fpSeedKeywords, string fpSuggestedKeywords)
		{
			this.FpTag = fpTag;
			this.FpCreatedOn = fpCreatedOn;
			this.FpCategory = fpCategory;
			this.FpKeywordBag = fpKeywordBag;
			this.FpProductList = fpProductList;
			this.FpKeywordPage1 = fpKeywordPage1;
			this.FpKeywordPage2 = fpKeywordPage2;
			this.FpKeywordPageOther = fpKeywordPageOther;
			this.FpSeedKeywords = fpSeedKeywords;
			this.FpSuggestedKeywords = fpSuggestedKeywords;
		}
		public override string ToString()
		{
			return "'" + this.FpTag + "','" + this.FpCreatedOn.ToString("yyyy-MM-dd") + "','" + this.FpCategory.Replace("'", "\\'") + "','" + this.FpKeywordBag.Replace("'", "\\'") + "','" + this.FpProductList.Replace("'", "\\'") + "','" + this.FpKeywordPage1.Replace("'", "\\'") + "','" + this.FpKeywordPage2.Replace("'", "\\'") + "','" + this.FpKeywordPageOther.Replace("'", "\\'") + "','" + this.FpSeedKeywords.Replace("'", "\\'") + "','" + this.FpSuggestedKeywords.Replace("'", "\\'") + "','" + this.LastUpdatedDate.ToString("yyyy-MM-dd") + "'";
		}
	}
}
