﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarketIntelligence
{
	public class KeywordModel
	{
		public double CostPerClick { get; set; } = 0;
		public string Keyword { get; set; } = null;
		public int[] Categories { get; set; } = null;
		public long AverageMonthlySearches { get; set; } = 0;
		public double Competition { get; set; } = 0;
		public string ExtractedFromWebpage { get; set; } = null;
		public string IdeaType { get; set; } = null;
		public List<MonthlySearchVolumeModel> TargetedMonthlySearches { get; set; } = null;
	}
	public class MonthlySearchVolumeModel
	{
		public long Count { get; set; }
		public int Month { get; set; }
		public int Year { get; set; }
	}
}
